#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>

using namespace std;
// funcion que cuenta las lineas, recibe el nombre del archivo y el total de lineas
void cuenta_lineas(char* archivo, int &total_lineas){
  // se abre el archivo, con el nombre que se recibe por parametro
  ifstream texto (archivo, ifstream::in);
  int lineas = 0;
  // se cuentan los saltos de linea en el archivo
  while (texto.good()) if(texto.get() == '\n') lineas++;
  cout << "Lineas: " << lineas << endl;
  // se le suman las lineas obtenidas a las totales
  total_lineas += lineas;
}
// funcion que cuenta los caracteress, recibe el nombre del archivo y el total de caracteres
void cuenta_caracteres(char* archivo, int &total_caracteres){
  int caracteres = 0;
  // se abre el archivo, con el nombre que se recibe por parametro
  ifstream texto (archivo, ifstream::in);
  // Se recorre el archivo hasta el final (EOF) y se cuentan los caracteres
  while (texto.good()) if(texto.get() != EOF) caracteres++;
  cout << "Caracteres: " << caracteres << endl;
  // Se le suman los caracteres obtenidos al total
  total_caracteres += caracteres;
}
// funcion que cuenta los caracteress, recibe el nombre del archivo y el total de caracteres
void cuenta_palabras(char* archivo, int &total_palabras) {
    // Se crea una cadena en la que se van a guardar las palabras y contarlas
  char cadena[10];
  int palabras = 0;
  // se abre el archivo, con el nombre que se recibe por parametro
  ifstream texto (archivo);
  // se recorre el archivo de texto hasta el final (eof)
  while(!texto.eof()){
    // cada vez que se guarda una palabra en la cadena se le sma uno a palabras
    texto >> cadena;
    palabras++;
  }
  // se cierra el archivo de texto
  texto.close();
  // Se le resta uno a palabra ya que siempre se cuenta una de mas
  palabras--;
  cout << "Palabras: " << palabras << endl;
  // Se le suma las palabras obtenidas al total de palabras
  total_palabras += palabras;

}

int main(int argc, char *archivos[]) {
  float tiempo_inicial, tiempo_final;
  // los contadores de la totalidad de datos
  int total_lineas = 0;
  int total_caracteres = 0;
  int total_palabras = 0;

  for(int i = 1; i <= argc -1 ;i++){
    // se recorre los datos entregados por parametros
    cout << "---Archivo: " << archivos[i] << "---" << endl;
    // se llama a las funciones
    tiempo_inicial = clock();
    cuenta_lineas(archivos[i], total_lineas);
    cuenta_caracteres(archivos[i], total_caracteres);
    cuenta_palabras(archivos[i], total_palabras);
    tiempo_final = clock();
  }
  // Se imprimen los totales
  cout << "Total de lineas entre los archivos: " << total_lineas << endl;
  cout << "Total de caracteres entre los archivos: " << total_caracteres << endl;
  cout << "Total de palabras entre los archivos: " << total_palabras << endl;
  cout << "El tiempo empleado es: " << (tiempo_final-tiempo_inicial)/CLOCKS_PER_SEC << "seg" << endl;
}
