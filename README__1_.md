# lab3_ssoo
*Lectura de archivos, secuencial y hebras*

***Pre-requisitos***

- C++
- Make

***Instalación***

- _Instalar Make._

    abrir terminal y escriba el siguiente codigo:
    $sudo apt install make


***Problematica***
- Crear dos programas, uno secuencias y el otro con hebras, los cuales reciben por parametro n nombres de archivos .txt, tras esto identificar: la cantidad de caracteres, la cantidad de palabras y la cantidad de lineas por cada archivo y posteriormente obtener el todal de caracteres, palabras y lineas, y calcular el tiempo empleado


***Ejecutando***

- Tanto el progrmama_secuencial como el programa_threads se ejecutan de la misma manera para ejecutar los programas debe tener todos los archivos (texto1 y texto2 son txt de prueba, puede usar los que usted quiera) de la carpeta del repositorio (progrmama_secuencial o programa_threads) en una carpeta del equipo, depues en la terminal usar el comando: **$_make_**, esperar que compile y ejecutar el programa con el siguiente comando: **_$./programa_** apompañado de los archivos empleazos separados por un espacio

como por ejemplo con los txt del repositorio: **_$./programa texto1.txt texto2.txt_**

***Construido con C++***

***Librerias***

- unistd.h
- fcntl.h
- pthread.h
- string.h
- iostream
- sstream
- fstream

***Version 0.1***

***Autor***

Luis Rebolledo





