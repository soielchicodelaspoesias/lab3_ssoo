#include <unistd.h>
#include <fcntl.h>
#include <pthread.h>
#include <string.h>
#include <iostream>
#include <sstream>
#include <fstream>
// Se declara los totales como variables globales
int total_lineas = 0;
int total_caracteres = 0;
int total_palabras = 0;
using namespace std;

// funcion que cuenta las lineas
void *cuenta_lineas(void *param){
  // se recibe el nombre del archivo
  char *archivo;
  archivo = (char *) param;
  // se abre el archivo
  ifstream texto (archivo);
  int lineas = 0;
  // se cuentan los saltos de lineas
  while (texto.good()) if(texto.get() == '\n') lineas++;
  cout << "Lineas: " << lineas << endl;
  // se suman las lineas obtenias al total de lineas
  total_lineas += lineas;
  // se cierra la hebra
  pthread_exit(0);

}
// Se cuentan los caracteres
void *cuenta_caracteres(void *param){
  // Se recibe el nombre del archivo
  char *archivo;
  archivo = (char *) param;
  int caracteres = 0;
  // Se abre el archivo de texto
  ifstream texto (archivo, ifstream::in);
  // Se cuentan los caracteres hasta el fin del archivo (EOF)
  while (texto.good()) if(texto.get() != EOF) caracteres++;
  cout << "Caracteres: " << caracteres << endl;
  // Se suman los caracteres obtenidos al total
  total_caracteres += caracteres;
  // Se cierra la hebra
  pthread_exit(0);
}
// Funcion que cuenta las palabras
void *cuenta_palabras(void *param) {
  // Se crea una cadena en la que se van a guardar las palabras
  char cadena[1];
  // Se obtiene el nombre del archivo
  char *archivo;
  archivo = (char *) param;
  int palabras = 0;
  // Abre el archivo
  ifstream texto (archivo);
  // Se recorren las palabras del archivo y se guardan en una variable
  while(!texto.eof()){
    texto >> cadena;
    // cada vez que se guarda una palabra en cadena se le suma uno a palabra
    palabras++;
  }
  // se cierra el archivo de texto
  texto.close();
  // se le resta uno a palabra por que se suma uno de mas
  palabras--;
  cout << "Palabras: " << palabras << endl;
  // se suman las palabras obtenidas al total
  total_palabras += palabras;
  // se cierra la hebra
  pthread_exit(0);

}

int main(int argc, char *archivos[]) {
  // Se cran las variables para calcular el tiempo empleado
  float tiempo_inicial_1, tiempo_inicial_2, tiempo_inicial_3;
  float tiempo_final_1, tiempo_final_2, tiempo_final_3;
  pthread_t threads[argc - 1];
  // Crea todos los hilos
  for (int i=0; i < argc - 1; i++) {
    cout << "---Archivo: " << archivos[i+1] << "---" << endl;
    sleep(0.001);
    // Se crea un hilo llamando a la funcion que crea los hilos y se le envia el nombre del archivo
    // un sleep por que se realiza muy rapido y no se imprime en la terminal
    // Se calula el tiempo inicial y el final
    tiempo_inicial_1 = clock();
    pthread_create(&threads[i], NULL, cuenta_lineas, archivos[i+1]);
    tiempo_final_1 = clock();
    // Se crea un hilo llamando a la funcion que crea los hilos y se le envia el nombre del archivo
    // un sleep por que se realiza muy rapido y no se imprime en la terminal
    // Se calula el tiempo inicial y el fina
    sleep(0.001);
    tiempo_inicial_2 = clock();
    pthread_create(&threads[i], NULL, cuenta_caracteres, archivos[i+1]);
    tiempo_final_2 = clock();
    sleep(0.001);
    // Se crea un hilo llamando a la funcion que crea los hilos y se le envia el nombre del archivo
    // un sleep por que se realiza muy rapido y no se imprime en la terminal
    // Se calula el tiempo inicial y el fina
    tiempo_inicial_3 = clock();
    pthread_create(&threads[i], NULL, cuenta_palabras, archivos[i+1]);
    tiempo_final_3 = clock();
    sleep(0.001);
  }
  // Cierra los hilos crados
  for (int i=0; i< argc - 1; i++) {
  pthread_join(threads[i], NULL);
  }
  // Se imprimen los totales
  cout << "Total de lineas entre los archivos: " << total_lineas << endl;
  cout << "Total de caracteres entre los archivos: " << total_caracteres << endl;
  cout << "Total de palabras entre los archivos: " << total_palabras << endl;
  // Se calcula el tiempo inicial
  float tiempo_inicial = tiempo_inicial_1 + tiempo_inicial_2 + tiempo_inicial_3;
  // Se calcula el tiempo final
  float tiempo_final = tiempo_final_1 + tiempo_final_2 + tiempo_final_3;
  // se calcula el tiempo y se imprime
  cout << "El tiempo empleado es: " << (tiempo_final-tiempo_inicial)/CLOCKS_PER_SEC << "seg" << endl;
}
